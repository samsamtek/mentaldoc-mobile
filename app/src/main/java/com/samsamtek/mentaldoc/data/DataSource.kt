package com.samsamtek.mentaldoc.data


import com.samsamtek.mentaldoc.data.source.remote.response.AccountResponse

interface DataSource {

    fun getDetailAccount(): AccountResponse?

//    fun getCourseWithModules(courseId: String): LiveData<Resource<CourseWithModule>>
//
//    fun getAllModulesByCourse(courseId: String): LiveData<Resource<List<ModuleEntity>>>
//
//    fun getContent(moduleId: String): LiveData<Resource<ModuleEntity>>
//
//    fun getBookmarkedCourses(): LiveData<List<CourseEntity>>
//
//    fun setCourseBookmark(course: CourseEntity, state: Boolean)
//
//    fun setReadModule(module: ModuleEntity)
}