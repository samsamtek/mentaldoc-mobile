package com.samsamtek.mentaldoc.data

import com.samsamtek.mentaldoc.data.source.remote.RemoteDataSource
import com.samsamtek.mentaldoc.data.source.remote.response.AccountResponse
import com.samsamtek.mentaldoc.utils.AppExecutors

class  Repository private constructor(
        private val remoteDataSource: RemoteDataSource,
        private val appExecutors: AppExecutors)
    : DataSource {

    companion object {
        @Volatile
        private var instance: Repository? = null

        fun getInstance(remoteData: RemoteDataSource, appExecutors: AppExecutors): Repository =
                instance ?: synchronized(this) {
                    Repository(remoteData, appExecutors).apply {
                        instance = this
                    }
                }
    }

    override fun getDetailAccount(): AccountResponse? {
        return remoteDataSource.getDetailAccount()
    }

//    override fun setCourseBookmark(course: CourseEntity, state: Boolean) =
//            appExecutors.diskIO().execute { localDataSource.setCourseBookmark(course, state) }

}

