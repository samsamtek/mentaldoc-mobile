package com.samsamtek.mentaldoc.data.source.remote

import com.samsamtek.mentaldoc.data.source.remote.response.*
import retrofit2.Call
import retrofit2.http.*

interface ApiService {

    @GET("{email}/account")
    fun getDetailAccount(
            @Path("email") email: String
    ): Call<AccountResponse>

    @GET("{email}/diary")
    fun getListDiary(
            @Path("email") email: String
    ): Call<List<DiaryResponse>>

    @GET("{email}/evaluation")
    fun getMonthlyEvaluation(
            @Path("email") email: String
    ): Call<EvaluationResponse>

    @GET("{email}/therapy")
    fun getIsNeedPsikiater(
            @Path("email") email: String
    ): Call<NeedPsikiaterResponse>

    @POST("register")
    fun registerAccount(
            @Body accountResponse: AccountResponse
    ): Call<SuccessResponse>

    @POST("{email}/diary")
    fun createDiary(
            @Path("email") email: String,
            @Body diaryResponse: DiaryResponse
    ): Call<SuccessResponse>
}
