package com.samsamtek.mentaldoc.data.source.remote

import android.os.Handler
import android.os.Looper
import com.samsamtek.mentaldoc.data.source.remote.response.AccountResponse

import android.util.Log
import retrofit2.Callback
import retrofit2.Call
import retrofit2.Response

class RemoteDataSource private constructor() {

    private val handler = Handler(Looper.getMainLooper())

    companion object {
        private const val TAG = "RemoteDataSource"
        private const val SERVICE_LATENCY_IN_MILLIS: Long = 2000

        @Volatile
        private var instance: RemoteDataSource? = null

        fun getInstance(): RemoteDataSource =
            instance ?: synchronized(this) {
                RemoteDataSource().apply { instance = this }
            }
    }

    fun getDetailAccount():AccountResponse? {
        var detailAccount: AccountResponse? = null
        val client = ApiConfig.getApiService().getDetailAccount("test@aa.aa")
        client.enqueue(object : Callback<AccountResponse> {
            override fun onResponse(
                call: Call<AccountResponse>,
                response: Response<AccountResponse>
            ) {
                if (response.isSuccessful) {
                    val account = response.body()
                    if (account != null) {
                        detailAccount = account
                    }
                } else {
                    Log.e(TAG, "onFailure: ${response.message()}")
                }
            }

            override fun onFailure(call: Call<AccountResponse>, t: Throwable) {
                Log.e(TAG, "onFailure: ${t.message.toString()}")
            }
        })
        return detailAccount
    }
}

