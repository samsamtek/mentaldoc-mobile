package com.samsamtek.mentaldoc.data.source.remote.response

import com.google.gson.annotations.SerializedName

data class AccountResponse(

	@field:SerializedName("birth_date")
	val birthDate: String,

	@field:SerializedName("name")
	val name: String,

	@field:SerializedName("email")
	val email: String,

	@field:SerializedName("password")
	val password: String,

	@field:SerializedName("last_education")
	val lastEducation: String
)
