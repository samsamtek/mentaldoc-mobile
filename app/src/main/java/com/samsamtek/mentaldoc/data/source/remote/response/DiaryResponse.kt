package com.samsamtek.mentaldoc.data.source.remote.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class DiaryResponse(

	@field:SerializedName("mood")
	val mood: String? = null,

	@field:SerializedName("list_factor")
	val listFactor: List<String?>? = null,

	@field:SerializedName("image_url")
	val imageUrl: String? = null,

	@field:SerializedName("image_base_64")
	val imageBase64: String? = null,

	@field:SerializedName("created_date")
	val createdDate: String? = null,

	@field:SerializedName("list_emotion")
	val listEmotion: List<String?>? = null,

	@field:SerializedName("diary_text")
	val diaryText: String? = null
) : Parcelable
