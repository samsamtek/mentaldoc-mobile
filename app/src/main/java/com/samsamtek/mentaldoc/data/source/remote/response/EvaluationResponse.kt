package com.samsamtek.mentaldoc.data.source.remote.response

import com.google.gson.annotations.SerializedName

data class EvaluationResponse(

	@field:SerializedName("list_frequency")
	val listFrequency: List<Int?>? = null
)
