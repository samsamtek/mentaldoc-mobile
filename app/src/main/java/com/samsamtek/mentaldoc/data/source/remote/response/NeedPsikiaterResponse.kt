package com.samsamtek.mentaldoc.data.source.remote.response

import com.google.gson.annotations.SerializedName

data class NeedPsikiaterResponse(

	@field:SerializedName("is_need_psikiater")
	val isNeedPsikiater: Boolean? = null
)
