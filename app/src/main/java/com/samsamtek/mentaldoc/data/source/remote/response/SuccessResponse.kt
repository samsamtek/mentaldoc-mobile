package com.samsamtek.mentaldoc.data.source.remote.response

import com.google.gson.annotations.SerializedName

data class SuccessResponse(

	@field:SerializedName("status")
	val status: String? = null
)
