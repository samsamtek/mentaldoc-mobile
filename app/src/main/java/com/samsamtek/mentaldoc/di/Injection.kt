package com.samsamtek.mentaldoc.di

import android.content.Context
import com.samsamtek.mentaldoc.data.Repository
import com.samsamtek.mentaldoc.data.source.remote.RemoteDataSource
import com.samsamtek.mentaldoc.utils.AppExecutors

object Injection {
    fun provideRepository(context: Context): Repository {
        val remoteDataSource = RemoteDataSource.getInstance()
        val appExecutors = AppExecutors()

        return Repository.getInstance(remoteDataSource, appExecutors)
    }
}
