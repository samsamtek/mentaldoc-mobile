package com.samsamtek.mentaldoc.ui.ui.creatediary

import android.app.PendingIntent.getActivity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import com.samsamtek.mentaldoc.R
import com.samsamtek.mentaldoc.databinding.ActivityCreateDiary1Binding



class CreateDiary1 : AppCompatActivity() {
    private var binding : ActivityCreateDiary1Binding? = null
    private var selectedMood : LinearLayout? = null
    private var aMood: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCreateDiary1Binding.inflate(layoutInflater)
        setContentView(binding!!.root)

        binding!!.creatediary1Toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        binding!!.btnNext1.isEnabled = false
        binding!!.moodGembira.setOnClickListener { setSelected(binding!!.moodGembira, "Gembira") }
        binding!!.moodMarah.setOnClickListener { setSelected(binding!!.moodMarah, "Marah") }
        binding!!.moodPenuhCinta.setOnClickListener { setSelected(binding!!.moodPenuhCinta, "Penuh Cinta") }
        binding!!.moodSedih.setOnClickListener { setSelected(binding!!.moodSedih, "Sedih") }
        binding!!.moodTakut.setOnClickListener { setSelected(binding!!.moodTakut, "Takut") }
        binding!!.btnNext1.setOnClickListener {
            var intent = Intent(this, CreateDiary2::class.java)
            var bundle = Bundle()
            bundle.putString("MOOD", aMood)
            intent.putExtras(bundle)
            startActivity(intent)
        }
    }

    fun setSelected(selected : LinearLayout, mood : String){
        if(this.selectedMood != null){
            this.selectedMood!!.background = ContextCompat.getDrawable(this, R.drawable.customborder);
        }
        this.selectedMood = selected
        this.selectedMood!!.background = ContextCompat.getDrawable(this, R.drawable.customborder_clicked);
        this.aMood = mood
        binding!!.btnNext1.isEnabled = true
    }
}