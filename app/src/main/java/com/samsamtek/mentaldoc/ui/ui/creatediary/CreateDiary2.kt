package com.samsamtek.mentaldoc.ui.ui.creatediary

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.samsamtek.mentaldoc.R
import com.samsamtek.mentaldoc.databinding.ActivityCreateDiary2Binding
import com.samsamtek.mentaldoc.ui.ui.home.ListDiaryAdapter
import com.samsamtek.mentaldoc.ui.ui.home.ListEmotionAdapter


class CreateDiary2 : AppCompatActivity() {
    private var binding : ActivityCreateDiary2Binding? = null
    private var adapter : ListEmotionAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCreateDiary2Binding.inflate(layoutInflater)
        setContentView(binding!!.root)

        var aMood = intent.getStringExtra("MOOD")
        setPage(aMood!!)

        binding!!.creatediary2Toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        disableButton()

        var list = arrayListOf<String>("Antusias", "Gembira", "Takjub", "Semangat", "Bangga", "Santai",
        "Tenang", "Puas", "Lega", "Marah", "Takut", "Stres", "Waspada", "Kewalah", "Kesal", "Malu", "Cemas", "Lesu", "Sedih", "Duka", "Bosan",
                "Apatis", "Kesepian", "Bingung"
                )
        this.adapter = ListEmotionAdapter(ArrayList(list), this)
        var container = GridLayoutManager(applicationContext, 3)
        binding?.rvEmotion?.layoutManager = container
        binding?.rvEmotion?.setHasFixedSize(true)
        binding?.rvEmotion?.adapter = this.adapter

        binding!!.btnNext2.setOnClickListener {


            var bundle = Bundle()
            bundle.putString("MOOD", intent.getStringExtra("MOOD"))
            bundle.putStringArrayList("LIST_EMOTION", this.adapter!!.selected)
            var intent = Intent(this, CreateDiary3::class.java)
            intent.putExtras(bundle)

            startActivity(intent)
        }
    }

    fun disableButton(){
        binding!!.btnNext2.isEnabled = false
    }

    fun enableButton(){
        binding!!.btnNext2.isEnabled = true
    }

    fun setPage(mood : String){
        if(mood == "Gembira"){
            binding!!.imgCreatediary2Mood.setImageResource(R.drawable.gembira)
            binding!!.tvCreatediary2Mood.setText("Gembira")
        }
        else if(mood == "Sedih"){
            binding!!.imgCreatediary2Mood.setImageResource(R.drawable.sedih)
            binding!!.tvCreatediary2Mood.setText("Sedih")
        }
        else if(mood == "Penuh Cinta"){
            binding!!.imgCreatediary2Mood.setImageResource(R.drawable.penuh_cinta)
            binding!!.tvCreatediary2Mood.setText("Penuh Cinta")
        }
        else if(mood == "Takut"){
            binding!!.imgCreatediary2Mood.setImageResource(R.drawable.takut)
            binding!!.tvCreatediary2Mood.setText("Takut")
        }
        else if(mood == "Marah"){
            binding!!.imgCreatediary2Mood.setImageResource(R.drawable.marah)
            binding!!.tvCreatediary2Mood.setText("Marah")
        }
    }
}