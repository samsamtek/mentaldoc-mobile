package com.samsamtek.mentaldoc.ui.ui.creatediary

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.samsamtek.mentaldoc.R
import com.samsamtek.mentaldoc.databinding.ActivityCreateDiary3Binding
import com.samsamtek.mentaldoc.ui.ui.home.ListEmotionAdapter
import com.samsamtek.mentaldoc.ui.ui.home.ListFactorAdapter

class CreateDiary3 : AppCompatActivity() {
    private var binding : ActivityCreateDiary3Binding? = null
    private var adapter : ListFactorAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCreateDiary3Binding.inflate(layoutInflater)
        setContentView(binding!!.root)

        binding!!.creatediary3Toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        disableButton()

        var list = arrayListOf<String>("Keluarga", "Pekerjaan",  "Teman", "Percintaan", "Kesehatan", "Pendidikan", "Tidur", "Makanan",
                "Hobi", "Cuaca", "Hiburan", "Keuangan", "Ibadah"
        )
        this.adapter = ListFactorAdapter(ArrayList(list), this)
        binding?.rvFactor?.layoutManager = LinearLayoutManager(this)
        binding?.rvFactor?.setHasFixedSize(true)
        binding?.rvFactor?.adapter = this.adapter

        binding!!.btnNext3.setOnClickListener {

            var map = this.adapter!!.map
            var factorList = arrayListOf<String>()
            for ((key, value) in map) {
                factorList.add(key)
                factorList.add(value.toString())
            }

            var bundle = Bundle()
            bundle.putString("MOOD", intent.getStringExtra("MOOD"))
            bundle.putStringArrayList("LIST_EMOTION", intent.getStringArrayListExtra("LIST_EMOTION"))
            bundle.putStringArrayList("LIST_FACTOR", factorList)
            var intent = Intent(this, CreateDiary4::class.java)
            intent.putExtras(bundle)
            startActivity(intent)
        }

    }

    fun disableButton(){
        binding!!.btnNext3.isEnabled = false
    }

    fun enableButton(){
        binding!!.btnNext3.isEnabled = true
    }
}