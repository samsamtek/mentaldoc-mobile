package com.samsamtek.mentaldoc.ui.ui.creatediary

import android.R
import android.R.attr
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64.DEFAULT
import android.util.Base64.encodeToString
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.samsamtek.mentaldoc.databinding.ActivityCreateDiary4Binding
import com.samsamtek.mentaldoc.ui.MainActivity
import com.samsamtek.mentaldoc.ui.ui.login.LoginActivity
import com.samsamtek.mentaldoc.ui.ui.register.RegisterViewModel
import com.samsamtek.mentaldoc.utils.UserPreference
import com.samsamtek.mentaldoc.viewmodel.ViewModelFactory
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class CreateDiary4 : AppCompatActivity() {
    private lateinit var createDiaryViewModel: CreateDiaryViewModel
    private var binding: ActivityCreateDiary4Binding? = null
    private var mStorageRef: StorageReference? = null
    private lateinit var imageUri: Uri
    private lateinit var base64Img: String
    private var pathDiaryImage: String = ""

//    override fun onClick(v: View?) {
//        when (v?.id) {
//            R.id.btn_submit -> {
//            }
//            R.id.btn_take_photo -> {
//            }
//        }
//    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == RESULT_OK && data != null && data.data != null) {
            imageUri = data.data!!

            var bitmap: Bitmap? = null
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, imageUri)
            } catch (e: IOException) {
                e.printStackTrace()
            }
            if (bitmap != null) {
                val imageBytes = imageToByteArray(bitmap)
                val encodedImage: String = encodeToString(
                    imageBytes,
                    DEFAULT
                ) // actual conversion to Base64 String Image
                base64Img = encodedImage
            }
            uploadPicture()
        }
    }

    fun getPicture() {
        val pd = ProgressDialog(this)
        pd.setTitle("Get Image ...")
        pd.show()

        val reference = mStorageRef?.child(pathDiaryImage)

//        reference?.downloadUrl?.addOnSuccessListener {
//            Glide.with(this)
//                    .load(it)
//                    .apply(RequestOptions().override(55, 55))
//                    .into(binding?.profilePic!!)
//            pd.dismiss()
//        }?.addOnFailureListener {
//            Toast.makeText(this, "Failed to get photo", Toast.LENGTH_LONG).show()
//        }
    }

    fun choosePicture() {
        intent = Intent()
        intent.setType("image/*")
        intent.setAction(Intent.ACTION_PICK)
        startActivityForResult(intent, 1)
    }

    fun uploadPicture() {
        val pd = ProgressDialog(this)
        pd.setTitle("Uploading Image ...")
        pd.show()

        val randomKey = UUID.randomUUID()
        pathDiaryImage = "diary-images/$randomKey"
        val reference = mStorageRef?.child(pathDiaryImage)
        reference?.putFile(imageUri!!)?.addOnSuccessListener {
            pd.dismiss()
        }?.addOnFailureListener {
            Toast.makeText(this, "Failed to upload photo", Toast.LENGTH_LONG)
        }
    }

    private fun imageToByteArray(bitmapImage: Bitmap): ByteArray? {
        val baos = ByteArrayOutputStream()
        bitmapImage.compress(Bitmap.CompressFormat.JPEG, 20, baos)
        return baos.toByteArray()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        this.getSupportActionBar()!!.setTitle("Tambah Diary");
        binding = ActivityCreateDiary4Binding.inflate(layoutInflater)
        setContentView(binding!!.root)

        binding!!.creatediary4Toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
//        binding!!.btnSubmitDiary.setOnClickListener{
//            if (binding!!.inputDiaryText.text.toString() != "") {
//                var mood = intent.getExtras()?.get("MOOD").toString()
//                var list_emotion = intent.getExtras()?.get("LIST_EMOTION") as List<String>
//                var list_factor = intent.getExtras()?.get("LIST_FACTOR") as List<String>
//                var diary_text = binding!!.inputDiaryText.text.toString()
//                var image_url = imageUri as String
//                var image_base_64 = base64Img
//
////                val userPreference = UserPreference(this)
////                val pass_local = userPreference.getPassword()
////                val email_local = userPreference.getEmail()
//
//                val sdf = SimpleDateFormat("yyyy-mm-dd")
//                val currentDate = sdf.format(Date())
//
//                Toast.makeText(this, "Loading...", Toast.LENGTH_LONG).show()
////                createDiaryViewModel.createDiary(email_local, mood, list_emotion, list_factor, diary_text, image_url, image_base_64, currentDate)
////                createDiaryViewModel.isSuccess.observe(this, { status ->
////                    if (status != null) {
////                        var intent = Intent(this, MainActivity::class.java)
////                        startActivity(intent)
////                    }
////                })
//            }
//        }
        binding!!.takePhoto.setOnClickListener{
            choosePicture()
        }
        mStorageRef = FirebaseStorage.getInstance().reference;

        createDiaryViewModel = obtainViewModel(this@CreateDiary4)
        setContentView(binding!!.root)
    }

    private fun obtainViewModel(activity: AppCompatActivity): CreateDiaryViewModel {
        val factory = ViewModelFactory.getInstance(
                this
        )
        return ViewModelProvider(activity, factory).get(CreateDiaryViewModel::class.java)
    }
}