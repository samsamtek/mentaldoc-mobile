package com.samsamtek.mentaldoc.ui.ui.creatediary

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.samsamtek.mentaldoc.data.Repository
import com.samsamtek.mentaldoc.data.source.remote.ApiConfig
import com.samsamtek.mentaldoc.data.source.remote.response.DiaryResponse
import retrofit2.Call
import com.samsamtek.mentaldoc.data.source.remote.response.SuccessResponse
import com.samsamtek.mentaldoc.ui.ui.register.RegisterViewModel
import com.samsamtek.mentaldoc.utils.UserPreference
import retrofit2.Callback
import retrofit2.Response

class CreateDiaryViewModel(private val repository: Repository) : ViewModel() {

    private val _text = MutableLiveData<String>().apply { value = "This is home Fragment" }
    val text: LiveData<String> = _text

    private val _isSuccess = MutableLiveData<SuccessResponse>()
    val isSuccess: LiveData<SuccessResponse> = _isSuccess
    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    fun createDiary(email:String, mood:String, list_emotion:List<String>, list_factor:List<String>, diary_text:String, image_url:String, image_base_64:String, date:String) {
        _isLoading.value = true
//        var listEmotion = listOf<String>("Antusias", "Gembira", "Semangat")
//        var listFactor = listOf<String>("Percintaan", "2", "Tidur", "5")
        var diary = DiaryResponse(mood, list_factor, image_url, image_base_64, date, list_emotion, diary_text)
        val client = ApiConfig.getApiService().createDiary(
            email, diary
        )
        client.enqueue(
            object : Callback<SuccessResponse> {
                override fun onResponse(
                    call: Call<SuccessResponse>,
                    response: Response<SuccessResponse>
                ) {
                    _isLoading.value = false
                    if (response.isSuccessful) {
                        _isSuccess.value = response.body()
                    } else {
                        Log.e("Debug", "onFailure: ${response.message()}")
                    }
                }

                override fun onFailure(call: Call<SuccessResponse>, t: Throwable) {
                    _isLoading.value = false
                    Log.e("Debug", "onFailure: ${t.message.toString()}")
                }
            })
    }
}
