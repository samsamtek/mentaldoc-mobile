package com.samsamtek.mentaldoc.ui.ui.home

import android.view.Gravity
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.samsamtek.mentaldoc.R
import android.app.Activity
import android.graphics.drawable.Drawable
import android.util.Log
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.samsamtek.mentaldoc.data.source.remote.response.DiaryResponse
import com.samsamtek.mentaldoc.databinding.ItemDiaryBinding
import com.samsamtek.mentaldoc.databinding.ItemEmotionBinding
import com.samsamtek.mentaldoc.ui.ui.creatediary.CreateDiary2
import java.util.*


class ListEmotionAdapter(private val list: ArrayList<String>, private val activity: CreateDiary2) : RecyclerView.Adapter<ListEmotionAdapter.ListViewHolder>() {

    private var listEmotion = list
    var selected : ArrayList<String> = arrayListOf()
//    private var onItemClickCallback: OnItemClickCallback? = null

    fun setListDiary(newlist: ArrayList<String>){
        this.listEmotion = newlist
    }

//    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
//        this.onItemClickCallback = onItemClickCallback
//    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ListViewHolder {
        val binding = ItemEmotionBinding.inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)
        return ListViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.bind(listEmotion[position])
    }

    override fun getItemCount(): Int = listEmotion.size

    inner class ListViewHolder(private val binding: ItemEmotionBinding) : RecyclerView.ViewHolder(binding.root) {
        internal fun bind(temp: String){
            binding.emotionName.text = temp
            itemView.setOnClickListener {
                if(selected.contains(temp)){
                    selected.remove(temp)
                    binding.emotionContainer.background = ContextCompat.getDrawable(activity, R.drawable.customborder);
                    if(selected.size == 0) activity.disableButton()
                }else{
                    selected.add(temp)
                    binding.emotionContainer.background = ContextCompat.getDrawable(activity, R.drawable.customborder_clicked);
                    activity.enableButton()
                }
            }
        }
    }

//    interface OnItemClickCallback {
//        fun onItemClicked(data: DiaryResponse)
//    }
}