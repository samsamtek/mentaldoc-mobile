package com.samsamtek.mentaldoc.ui.ui.home

import android.view.Gravity
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.SeekBar
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.samsamtek.mentaldoc.R
import com.samsamtek.mentaldoc.databinding.ItemFactorsBinding
import com.samsamtek.mentaldoc.ui.ui.creatediary.CreateDiary3
import java.util.*
import kotlin.collections.HashMap


class ListFactorAdapter(private val list: ArrayList<String>, private val activity : CreateDiary3) : RecyclerView.Adapter<ListFactorAdapter.ListViewHolder>() {

    private var listString = list
    var map : HashMap<String, Int> = HashMap<String, Int>()
//    private var onItemClickCallback: OnItemClickCallback? = null

    fun setListDiary(newlist: ArrayList<String>){
        this.listString = newlist
    }

//    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
//        this.onItemClickCallback = onItemClickCallback
//    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ListViewHolder {
        val binding = ItemFactorsBinding.inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)
        return ListViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.bind(listString[position])
    }

    override fun getItemCount(): Int = listString.size

    inner class ListViewHolder(private val binding: ItemFactorsBinding) : RecyclerView.ViewHolder(binding.root) {
        internal fun bind(temp: String){
            binding.checkBox.text = temp
            binding.seekBar.setMax(0);
            binding.seekBar.setMax(5);

            binding.checkBox.setOnCheckedChangeListener { buttonView, isChecked ->
                if(isChecked){
                    binding.seekBar.isEnabled = true
                    binding.factorContainer.background = ContextCompat.getDrawable(activity, R.drawable.customborder_clicked);
                    if(!map.containsKey(binding.checkBox.text)) map.put(binding.checkBox.text.toString(), 0)
                    activity.enableButton()
                }else{
                    binding.seekBar.isEnabled = false
                    binding.seekBar.setProgress(0)
                    binding.factorContainer.background = ContextCompat.getDrawable(activity, R.drawable.customborder);
                    if(map.containsKey(binding.checkBox.text)) map.remove(binding.checkBox.text)
                    if(map.size == 0) activity.disableButton()
                }
            }

            binding.seekBar.setOnSeekBarChangeListener(
                    object : SeekBar.OnSeekBarChangeListener {

                        override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                            map.put(binding.checkBox.text.toString(), i)
                        }

                        override fun onStartTrackingTouch(seekBar: SeekBar?) {
                        }

                        override fun onStopTrackingTouch(seekBar: SeekBar?) {
                        }
                    }
            )

            binding.seekBar.isEnabled = false
        }

    }


//    interface OnItemClickCallback {
//        fun onItemClicked(data: DiaryResponse)
//    }
}



