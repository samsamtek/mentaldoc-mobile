package com.samsamtek.mentaldoc.ui.ui.dashboard

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.samsamtek.mentaldoc.R
import com.samsamtek.mentaldoc.databinding.FragmentDashboardBinding
import com.samsamtek.mentaldoc.viewmodel.ViewModelFactory

class DashboardFragment : Fragment() {

    private lateinit var dashboardViewModel: DashboardViewModel
    private var binding : FragmentDashboardBinding? = null
    private var listMood = arrayListOf<String>("Gembira", "Marah", "Penuh Cinta", "Takut", "Sedih")
    private var listFreq = arrayListOf<Int?>()
    private var selected = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDashboardBinding.inflate(inflater, container, false)
        return binding!!.getRoot()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val factory = ViewModelFactory.getInstance(requireActivity())
        dashboardViewModel =
                ViewModelProvider(this,factory).get(DashboardViewModel::class.java)

        beforeSuccess()
        binding!!.progressBarDashboard.visibility = View.VISIBLE
        dashboardViewModel.getEvaluation()
        dashboardViewModel.evaluation.observe(viewLifecycleOwner, Observer {
            Log.d("DEBUG", it.listFrequency.toString())
            if(it != null){
                this.listFreq = ArrayList(it.listFrequency)
                setPage(listMood.get(0))
                binding!!.tvDashboardDesc.text = "Pada bulan ini, kamu mengalami mood ${listMood.get(0)} sebanyak"
                binding!!.tvDashboardCount.text = "${it.listFrequency?.get(0)} x"
                checkStatus()
                afterSuccess()
                binding!!.progressBarDashboard.visibility = View.INVISIBLE
            }
        })
    }

    fun beforeSuccess(){
        binding!!.imgDashboardMood.visibility = View.INVISIBLE
        binding!!.tvDashboardDesc.visibility = View.INVISIBLE
        binding!!.tvDashboardMood.visibility = View.INVISIBLE
        binding!!.tvDashboardCount.visibility = View.INVISIBLE
        binding!!.btnDashboardDec.visibility = View.INVISIBLE
        binding!!.btnDashboardInc.visibility = View.INVISIBLE
    }

    fun afterSuccess(){
        binding!!.imgDashboardMood.visibility = View.VISIBLE
        binding!!.tvDashboardDesc.visibility = View.VISIBLE
        binding!!.tvDashboardMood.visibility = View.VISIBLE
        binding!!.tvDashboardCount.visibility = View.VISIBLE
        binding!!.btnDashboardDec.visibility = View.VISIBLE
        binding!!.btnDashboardInc.visibility = View.VISIBLE
        binding!!.btnDashboardInc.setOnClickListener {
            selected = selected + 1
            setLayout(selected)
            checkStatus()
        }
        binding!!.btnDashboardDec.setOnClickListener {
            selected = selected - 1
            setLayout(selected)
            checkStatus()
        }
    }

    fun setLayout(i: Int){
        setPage(listMood.get(i))
        binding!!.tvDashboardDesc.text = "Pada bulan ini, kamu mengalami mood ${listMood.get(i)} sebanyak"
        binding!!.tvDashboardCount.text = "${this.listFreq.get(i)} x"
    }

    fun checkStatus(){
        if(this.selected == 0) {
            binding!!.btnDashboardDec.isEnabled = false
            binding!!.btnDashboardInc.isEnabled = true

            binding!!.btnDashboardDec.background = ContextCompat.getDrawable(requireActivity(), R.drawable.customborder_disabled);
            binding!!.btnDashboardInc.background = ContextCompat.getDrawable(requireActivity(), R.drawable.customborder);
        }
        else if(this.selected == 4) {
            binding!!.btnDashboardDec.isEnabled = true
            binding!!.btnDashboardInc.isEnabled = false

            binding!!.btnDashboardDec.background = ContextCompat.getDrawable(requireActivity(), R.drawable.customborder);
            binding!!.btnDashboardInc.background = ContextCompat.getDrawable(requireActivity(), R.drawable.customborder_disabled);
        }else{
            binding!!.btnDashboardDec.isEnabled = true
            binding!!.btnDashboardInc.isEnabled = true

            binding!!.btnDashboardDec.background = ContextCompat.getDrawable(requireActivity(), R.drawable.customborder);
            binding!!.btnDashboardInc.background = ContextCompat.getDrawable(requireActivity(), R.drawable.customborder);
        }
    }

    fun setPage(mood : String){
        if(mood == "Gembira"){
            binding!!.imgDashboardMood.setImageResource(R.drawable.gembira)
            binding!!.tvDashboardMood.setText("Gembira")
        }
        else if(mood == "Sedih"){
            binding!!.imgDashboardMood.setImageResource(R.drawable.sedih)
            binding!!.tvDashboardMood.setText("Sedih")
        }
        else if(mood == "Penuh Cinta"){
            binding!!.imgDashboardMood.setImageResource(R.drawable.penuh_cinta)
            binding!!.tvDashboardMood.setText("Penuh Cinta")
        }
        else if(mood == "Takut"){
            binding!!.imgDashboardMood.setImageResource(R.drawable.takut)
            binding!!.tvDashboardMood.setText("Takut")
        }
        else if(mood == "Marah"){
            binding!!.imgDashboardMood.setImageResource(R.drawable.marah)
            binding!!.tvDashboardMood.setText("Marah")
        }
    }
}