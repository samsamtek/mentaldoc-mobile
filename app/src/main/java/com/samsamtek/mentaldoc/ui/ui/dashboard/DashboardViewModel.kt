package com.samsamtek.mentaldoc.ui.ui.dashboard

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.samsamtek.mentaldoc.data.Repository
import com.samsamtek.mentaldoc.data.source.remote.ApiConfig
import com.samsamtek.mentaldoc.data.source.remote.response.EvaluationResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DashboardViewModel(private val repository: Repository) : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is dashboard Fragment"
    }
    val text: LiveData<String> = _text

    private val _evaluation = MutableLiveData<EvaluationResponse>()
    val evaluation: LiveData<EvaluationResponse> = _evaluation
    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    fun getEvaluation() {
        _isLoading.value = true
        val client = ApiConfig.getApiService().getMonthlyEvaluation("test@aa.aa")
        client.enqueue(object : Callback<EvaluationResponse> {
            override fun onResponse(
                call: Call<EvaluationResponse>,
                response: Response<EvaluationResponse>
            ) {
                if (response.isSuccessful) {
                    val evaluationRes = response.body()
                    if (evaluationRes != null){
                        _evaluation.value = evaluationRes!!
                    }
                } else {
                    Log.e("Debug", "onFailure: ${response.message()}")
                }
            }

            override fun onFailure(call: Call<EvaluationResponse>, t: Throwable) {
                Log.e("Debug", "onFailure: ${t.message.toString()}")
            }
        })
    }
}