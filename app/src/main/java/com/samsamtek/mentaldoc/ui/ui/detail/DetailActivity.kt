package com.samsamtek.mentaldoc.ui.ui.detail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.samsamtek.mentaldoc.data.source.remote.response.DiaryResponse
import com.samsamtek.mentaldoc.databinding.ActivityDetailBinding

class DetailActivity : AppCompatActivity() {
    private var binding : ActivityDetailBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding!!.root)
        var detail = intent.getParcelableExtra<DiaryResponse>("DETAIL") as DiaryResponse

        binding!!.detailToolbar.title = detail.createdDate
    }
}