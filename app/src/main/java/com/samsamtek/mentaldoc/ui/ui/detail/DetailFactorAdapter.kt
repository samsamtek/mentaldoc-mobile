//package com.samsamtek.mentaldoc.ui.ui.home
//
//import android.view.Gravity
//import android.view.LayoutInflater
//import android.view.ViewGroup
//import android.widget.FrameLayout
//import androidx.core.content.ContextCompat
//import androidx.recyclerview.widget.RecyclerView
//import com.samsamtek.mentaldoc.R
//import com.samsamtek.mentaldoc.databinding.ItemEmotionBinding
//import com.samsamtek.mentaldoc.ui.ui.creatediary.CreateDiary2
//import com.samsamtek.mentaldoc.ui.ui.detail.DetailActivity
//import java.util.*
//
//
//class DetailFactorAdapter(private val list: ArrayList<String>) : RecyclerView.Adapter<DetailFactorAdapter.ListViewHolder>() {
//
//    private var listFactor = list
//    var selected : ArrayList<String> = arrayListOf()
//
//    fun setListDiary(newlist: ArrayList<String>){
//        this.listEmotion = newlist
//    }
//
//    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ListViewHolder {
//        val binding = ItemEmotionBinding.inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)
//        return ListViewHolder(binding)
//    }
//
//    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
//        holder.bind(listEmotion[position])
//    }
//
//    override fun getItemCount(): Int = listEmotion.size
//
//    inner class ListViewHolder(private val binding: ItemEmotionBinding) : RecyclerView.ViewHolder(binding.root) {
//        internal fun bind(temp: String){
//            binding.emotionName.text = temp
//        }
//    }
//}