package com.samsamtek.mentaldoc.ui.ui.detail

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Factor(
        val name: String? = null,
        val level: Int? = 0,
) : Parcelable
