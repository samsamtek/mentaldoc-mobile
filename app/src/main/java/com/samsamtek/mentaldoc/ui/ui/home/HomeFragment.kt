package com.samsamtek.mentaldoc.ui.ui.home

import android.content.Intent
import android.content.Intent.getIntent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.samsamtek.mentaldoc.R
import com.samsamtek.mentaldoc.data.source.remote.response.DiaryResponse
import com.samsamtek.mentaldoc.databinding.FragmentHomeBinding
import com.samsamtek.mentaldoc.ui.ui.creatediary.CreateDiary1
import com.samsamtek.mentaldoc.viewmodel.ViewModelFactory


class HomeFragment : Fragment(), View.OnClickListener {

    private lateinit var homeViewModel: HomeViewModel
    private var binding : FragmentHomeBinding? = null
    private var adapter : ListDiaryAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding!!.getRoot()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val factory = ViewModelFactory.getInstance(requireActivity())
        homeViewModel =
                ViewModelProvider(this, factory).get(HomeViewModel::class.java)
//        binding = FragmentHomeBinding.inflate(layoutInflater)

        homeViewModel.getListDiaryResponse()
        homeViewModel.listDiaryResponse.observe(requireActivity(), { list ->
            binding!!.progressBarHome.visibility = View.VISIBLE
            if (list != null) {
                if (this.adapter == null) {
                    setUp(list)
                } else {
                    this.adapter!!.setListDiary(ArrayList(list))
                    this.adapter!!.notifyDataSetChanged()
                }
                binding!!.progressBarHome.visibility = View.INVISIBLE
//                if(list.size == 0) binding?.tvNoData?.setText("Tidak Ada Data")
//                else binding?.tvNoData?.setText("")
            }
        })
        binding!!.btnCreate.setOnClickListener(this)
        binding!!.btnToNotification.setOnClickListener(this)
    }

    fun setUp(list: List<DiaryResponse>){
        this.adapter = ListDiaryAdapter(ArrayList(list), requireActivity())
        binding?.rvHomeDiary?.layoutManager = LinearLayoutManager(activity)
        binding?.rvHomeDiary?.setHasFixedSize(true)
        binding?.rvHomeDiary?.adapter = this.adapter
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_create -> {
                val intent = Intent(activity, CreateDiary1::class.java)
                activity?.startActivity(intent)
            }
            R.id.btn_to_notification -> {
                val navController = requireActivity().findNavController(R.id.nav_host_fragment)
                navController.navigate(R.id.navigation_notifications)
            }
        }
    }
}