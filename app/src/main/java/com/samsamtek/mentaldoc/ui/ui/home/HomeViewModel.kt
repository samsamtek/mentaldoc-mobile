package com.samsamtek.mentaldoc.ui.ui.home

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.samsamtek.mentaldoc.data.Repository
import com.samsamtek.mentaldoc.data.source.remote.ApiConfig
import com.samsamtek.mentaldoc.data.source.remote.response.DiaryResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeViewModel(private val repository: Repository) : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val text: LiveData<String> = _text

    private val _listDiaryResponse = MutableLiveData<List<DiaryResponse>>()
    val listDiaryResponse: LiveData<List<DiaryResponse>> = _listDiaryResponse
    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

//    list diary

    fun getListDiaryResponse() {
        _isLoading.value = true
        val client = ApiConfig.getApiService().getListDiary("test@aa.aa")
        client.enqueue(object : Callback<List<DiaryResponse>> {
            override fun onResponse(
                call: Call<List<DiaryResponse>>,
                response: Response<List<DiaryResponse>>
            ) {
                if (response.isSuccessful) {
                    val listDiaryRes = response.body()
                    if (listDiaryRes != null){
                        _listDiaryResponse.value = listDiaryRes!!
                    }
                } else {
                    Log.e("Debug", "onFailure: ${response.message()}")
                }
            }

            override fun onFailure(call: Call<List<DiaryResponse>>, t: Throwable) {
                Log.e("Debug", "onFailure: ${t.message.toString()}")
            }
        })
    }

}