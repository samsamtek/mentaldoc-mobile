package com.samsamtek.mentaldoc.ui.ui.home

import android.content.Intent
import android.graphics.drawable.Drawable
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.samsamtek.mentaldoc.R
import com.samsamtek.mentaldoc.data.source.remote.response.DiaryResponse
import com.samsamtek.mentaldoc.databinding.ItemDiaryBinding
import com.samsamtek.mentaldoc.ui.MainActivity
import com.samsamtek.mentaldoc.ui.ui.detail.DetailActivity
import java.util.*

class ListDiaryAdapter(private val list: ArrayList<DiaryResponse>, private val activity : FragmentActivity) : RecyclerView.Adapter<ListDiaryAdapter.ListViewHolder>() {

    private var listDiary = list
//    private var onItemClickCallback: OnItemClickCallback? = null

    fun setListDiary (newlist: ArrayList<DiaryResponse>){
        this.listDiary = newlist
    }

//    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
//        this.onItemClickCallback = onItemClickCallback
//    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ListViewHolder {
        val binding = ItemDiaryBinding.inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)
        return ListViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.bind(listDiary[position])
    }

    override fun getItemCount(): Int = listDiary.size

    inner class ListViewHolder(private val binding: ItemDiaryBinding) : RecyclerView.ViewHolder(binding.root) {
        internal fun bind(temp: DiaryResponse){
            binding.tvItemName.text = temp.createdDate
            binding.tvItemShortdesc.text = temp.mood
            var img: Int? = null
            if(temp.mood.toString().toLowerCase() == "gembira") img = R.drawable.gembira
            else if(temp.mood.toString().toLowerCase() == "sedih") img = R.drawable.sedih
            else if(temp.mood.toString().toLowerCase() == "takut") img = R.drawable.takut
            else if(temp.mood.toString().toLowerCase() == "penuh_cinta") img = R.drawable.penuh_cinta
            else if(temp.mood.toString().toLowerCase() == "marah") img = R.drawable.marah
            Glide.with(this.itemView)
                    .load(img)
                    .apply(RequestOptions().override(55, 55))
                    .into(binding.imgItemPhoto)
            itemView.setOnClickListener {
                var intent  = Intent(activity, DetailActivity::class.java)
                intent.putExtra("DETAIL", temp)
                activity.startActivity(intent)
            }
        }
    }

//    interface OnItemClickCallback {
//        fun onItemClicked(data: DiaryResponse)
//    }
}