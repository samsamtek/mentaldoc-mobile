package com.samsamtek.mentaldoc.ui.ui.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.samsamtek.mentaldoc.AlarmReceiver
import com.samsamtek.mentaldoc.R
import com.samsamtek.mentaldoc.databinding.ActivityLoginBinding
import com.samsamtek.mentaldoc.ui.MainActivity
import com.samsamtek.mentaldoc.ui.ui.register.RegisterActivity
import com.samsamtek.mentaldoc.utils.UserPreference
import com.samsamtek.mentaldoc.viewmodel.ViewModelFactory

class LoginActivity : AppCompatActivity(), View.OnClickListener {
    private var binding: ActivityLoginBinding? = null
    private lateinit var alarmReceiver: AlarmReceiver
    private lateinit var loginViewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        binding!!.btnLogin.setOnClickListener(this)
        binding!!.btnRegister.setOnClickListener(this)

        loginViewModel = obtainViewModel(this@LoginActivity)

        setContentView(binding!!.root)
        alarmReceiver = AlarmReceiver()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_login -> {
                login()
            }
            R.id.btn_register -> {
                val intent = Intent(this, RegisterActivity::class.java)
                startActivity(intent)
            }
        }
    }

    private fun obtainViewModel(activity: AppCompatActivity): LoginViewModel {
        val factory = ViewModelFactory.getInstance(this)
        return ViewModelProvider(activity, factory).get(LoginViewModel::class.java)
    }

    fun login() {
        val pass_input = binding!!.inputLoginPassword.text.toString()
        val email_input = binding!!.inputLoginEmail.text.toString()

        val userPreference = UserPreference(this)
        val pass_db = userPreference.getPassword()
        val email_db = userPreference.getEmail()

        if (pass_db == "" && email_db == "") {
            loginViewModel.getAccount(email_input)
        } else {
            checkEmailPass(email_input, pass_input)
            return
        }

        loginViewModel.isLoading.observe(
                this,
                {
                    if (it == true) {
                        // TODO: loading indicator on
                    } else {
                        // TODO: loading indicator off
                    }
                })

        loginViewModel.account.observe(
                this,
                {
                    if (it != null) {
                        val userPreference = UserPreference(this)
                        val pass = it.password.split("notherallyhashed")[0]
                        userPreference.setEmailPass(it.email, pass)
                        checkEmailPass(email_input, pass_input)
                    }
                })
    }

    fun checkEmailPass(email: String, pass: String) {
        val userPreference = UserPreference(this)
        val pass_local = userPreference.getPassword()
        val email_local = userPreference.getEmail()

        if (pass_local != pass || email_local != email) {
            Toast.makeText(this, "Your email or password did not match...", Toast.LENGTH_LONG)
                    .show()
            return
        }
        alarmReceiver.setRepeatingAlarm(this, "Click here for the details")
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        return
    }
}
