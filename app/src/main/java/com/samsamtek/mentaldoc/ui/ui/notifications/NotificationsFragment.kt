package com.samsamtek.mentaldoc.ui.ui.notifications

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.samsamtek.mentaldoc.databinding.FragmentNotificationsBinding
import com.samsamtek.mentaldoc.viewmodel.ViewModelFactory
import com.samsamtek.mentaldoc.AlarmReceiver
import java.util.*

class NotificationsFragment : Fragment() {

    private lateinit var notificationsViewModel: NotificationsViewModel
    private var binding : FragmentNotificationsBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentNotificationsBinding.inflate(inflater, container, false)
        return binding!!.getRoot()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val factory = ViewModelFactory.getInstance(requireActivity())
        notificationsViewModel =
                ViewModelProvider(this, factory).get(NotificationsViewModel::class.java)


//        notificationsViewModel.
//        homeViewModel.listDiaryResponse.observe(requireActivity(), { list ->
//            binding!!.progressBarHome.visibility = View.VISIBLE
//            if (list != null) {
//                if(this.adapter == null){
//                    setUp(list)
//                }else{
//                    this.adapter!!.setListDiary(ArrayList(list))
//                    this.adapter!!.notifyDataSetChanged()
//                }
//                binding!!.progressBarHome.visibility = View.INVISIBLE
////                if(list.size == 0) binding?.tvNoData?.setText("Tidak Ada Data")
////                else binding?.tvNoData?.setText("")
//            }
//        })
    }
}