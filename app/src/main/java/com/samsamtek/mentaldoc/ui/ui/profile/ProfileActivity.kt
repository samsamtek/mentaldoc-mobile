package com.samsamtek.mentaldoc.ui.ui.profile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.samsamtek.mentaldoc.R

class ProfileActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
    }
}