package com.samsamtek.mentaldoc.ui.ui.profile

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.samsamtek.mentaldoc.data.Repository
import com.samsamtek.mentaldoc.data.source.remote.ApiConfig
import com.samsamtek.mentaldoc.data.source.remote.response.DiaryResponse
import com.samsamtek.mentaldoc.data.source.remote.response.AccountResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProfileViewModel(private val repository: Repository) : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val text: LiveData<String> = _text

    private val _account = MutableLiveData<AccountResponse>()
    val account: LiveData<AccountResponse> = _account
    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

//    list diary

    fun getDetailAccount() {
        _isLoading.value = true
        val client = ApiConfig.getApiService().getDetailAccount("test@aa.aa")
        client.enqueue(object : Callback<AccountResponse> {
            override fun onResponse(
                call: Call<AccountResponse>,
                response: Response<AccountResponse>
            ) {
                if (response.isSuccessful) {
                    val accountRes = response.body()
                    if (accountRes != null){
                        _account.value = accountRes!!
                    }
                } else {
                    Log.e("Debug", "onFailure: ${response.message()}")
                }
            }

            override fun onFailure(call: Call<AccountResponse>, t: Throwable) {
                Log.e("Debug", "onFailure: ${t.message.toString()}")
            }
        })
    }

}