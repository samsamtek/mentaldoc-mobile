package com.samsamtek.mentaldoc.ui.ui.register

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.samsamtek.mentaldoc.databinding.ActivityRegisterBinding
import com.samsamtek.mentaldoc.ui.ui.login.LoginActivity
import com.samsamtek.mentaldoc.utils.UserPreference
import com.samsamtek.mentaldoc.viewmodel.ViewModelFactory
import java.text.SimpleDateFormat
import java.util.*


class RegisterActivity : AppCompatActivity(){
    private var binding : ActivityRegisterBinding? = null
    private var datePickerDialog: DatePickerDialog? = null
    private var dateFormatter: SimpleDateFormat? = null
    private lateinit var registerViewModel: RegisterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityRegisterBinding.inflate(layoutInflater)
        binding!!.registerToolbar.setNavigationOnClickListener { onBackPressed() }

        val dropdown = binding!!.spinner
        val items = arrayOf("SD", "SMP", "SMA", "Strata 1", "Strata 2", "Strata 3")
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, items)
        dropdown.adapter = adapter

        binding!!.btnBuatAkun.setOnClickListener { checkInput() }
        binding!!.btnBatal.setOnClickListener { onBackPressed() }
        dateFormatter = SimpleDateFormat("dd-MM-yyyy", Locale.US)
        binding!!.btnDatepicker.setOnClickListener{ showDateDialog() }

        registerViewModel = obtainViewModel(this@RegisterActivity)

        setContentView(binding!!.root)
    }

    private fun obtainViewModel(activity: AppCompatActivity): RegisterViewModel {
        val factory = ViewModelFactory.getInstance(
                this
        )
        return ViewModelProvider(activity, factory).get(RegisterViewModel::class.java)
    }

    private fun showDateDialog() {
        val newCalendar: Calendar = Calendar.getInstance()
        datePickerDialog = DatePickerDialog(this, { view, year, monthOfYear, dayOfMonth ->
            val newDate: Calendar = Calendar.getInstance()
            newDate.set(year, monthOfYear, dayOfMonth)
            binding!!.tvPickedDate.setText(dateFormatter!!.format(newDate.getTime()))
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH))
        datePickerDialog!!.show()
    }

    fun checkInput(){
        var bEmail = binding!!.inputRegisterEmail.text.toString()  != ""
        var bPassword = binding!!.inputRegisterPassword.text.toString()  != ""
        var bConfPassword = binding!!.inputRegisterConfirmationPassword.text.toString()  != ""
        var bMatchPassword = binding!!.inputRegisterPassword.text.toString() == binding!!.inputRegisterConfirmationPassword.text.toString()
        var bName = binding!!.inputRegisterName.text.toString()  != ""
        var bBirth = binding!!.tvPickedDate.text.toString()  != ""
        var bEducation = binding!!.spinner
                .getSelectedItem().toString() != ""

        if(bEmail && bPassword && bConfPassword && bName && bBirth && bEducation){
            if( !bMatchPassword ){
                Toast.makeText(this, "Konfirmasi Password Salah", Toast.LENGTH_LONG)
                    .show()
                Log.d("DEBUG", "if if")
                Log.d("DEBUG", binding!!.inputRegisterPassword.text.toString())
                Log.d("DEBUG", binding!!.inputRegisterConfirmationPassword.text.toString())
            }else{
                val userPreference = UserPreference(this)
                userPreference.setEmailPass(binding!!.inputRegisterEmail.text.toString(), binding!!.inputRegisterPassword.text.toString())

                val email = binding!!.inputRegisterEmail.text.toString()
                val password = binding!!.inputRegisterPassword.text.toString()
                val name = binding!!.inputRegisterName.text.toString()
                val birth_date = binding!!.tvPickedDate.text.toString()
                val last_education = binding!!.spinner.getSelectedItem().toString()

                //nge post
                Log.d("DEBUG", "if else")
                Toast.makeText(this, "Loading...", Toast.LENGTH_LONG)
                    .show()
                registerViewModel.registerAccount(email, password, name, changeDate(birth_date), last_education)
                registerViewModel.isSuccess.observe(this, { status ->
                    if (status != null) {
                        var intent = Intent(this, LoginActivity::class.java)
                        startActivity(intent)
                    }
                })
            }
        }else{
            Log.d("DEBUG", "else")
            Toast.makeText(this, "Lengkapi Formulir Anda", Toast.LENGTH_LONG)
        }
    }

    fun changeDate(old:String): String{
        var new = ""
        new += old.substring(6,10)
        new += "-"
        new += old.substring(3,5)
        new += "-"
        new += old.substring(0,2)
        return new
    }
}