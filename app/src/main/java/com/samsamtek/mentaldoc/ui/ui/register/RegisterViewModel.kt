package com.samsamtek.mentaldoc.ui.ui.register

import android.accounts.Account
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.samsamtek.mentaldoc.data.Repository
import com.samsamtek.mentaldoc.data.source.remote.ApiConfig
import com.samsamtek.mentaldoc.data.source.remote.response.AccountResponse
import retrofit2.Call
import com.samsamtek.mentaldoc.data.source.remote.response.SuccessResponse
import retrofit2.Callback
import retrofit2.Response

class RegisterViewModel(private val repository: Repository) : ViewModel() {

    private val _text = MutableLiveData<String>().apply { value = "This is home Fragment" }
    val text: LiveData<String> = _text

    private val _isSuccess = MutableLiveData<SuccessResponse>()
    val isSuccess: LiveData<SuccessResponse> = _isSuccess
    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    fun registerAccount(email:String, password:String, name:String, birth_date:String, last_education:String) {
        _isLoading.value = true
        email.trim()
        password.trim()
        name.trim()
        birth_date.trim()
        last_education.trim()
        val acc = AccountResponse(
                birth_date,
                name,
                email,
                password,
                last_education
        )
        val client = ApiConfig.getApiService()
                .registerAccount(acc)
        client.enqueue(
                object : Callback<SuccessResponse> {
                    override fun onResponse(
                            call: Call<SuccessResponse>,
                            response: Response<SuccessResponse>
                    ) {
                        _isLoading.value = false
                        if (response.isSuccessful) {
                            _isSuccess.value = response.body()
                        } else {
                            Log.e("Debug", "onFailure: ${response.message()}")
                        }
                    }

                    override fun onFailure(call: Call<SuccessResponse>, t: Throwable) {
                        _isLoading.value = false
                        Log.e("Debug", "onFailure: ${t.message.toString()}")
                    }
                })
    }
}
