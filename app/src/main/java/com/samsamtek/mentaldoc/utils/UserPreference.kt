package com.samsamtek.mentaldoc.utils

import android.content.Context
import com.samsamtek.mentaldoc.data.source.remote.response.AccountResponse

internal class UserPreference(context: Context) {
    companion object {
        private const val PREFS_NAME = "user_pref"
        private const val EMAIL = "email"
        private const val PASSWORD = "password"
    }

    private val preferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
    fun setEmailPass(email: String, password: String) {
        val editor = preferences.edit()
        editor.putString(EMAIL, email)
        editor.putString(PASSWORD, password)
        editor.apply()
    }
    fun getPassword(): String{
        val pass = preferences.getString(PASSWORD, "")
        return pass!!
    }
    fun getEmail(): String{
        val email = preferences.getString(EMAIL, "")
        return email!!
    }
}
