package com.samsamtek.mentaldoc.vo


enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
